import os
import random
import string
import subprocess
import json
import logging

configuration = {'name': 'pylxd', 'source': {'type': 'image', 'alias': 'ubuntu/20.04'},
                 'config': {'limits.cpu': '2', 'security.nesting': 'ture', 'security.privileged': 'ture'}}


class ConfigBuilder:

    def __init__(self):
        self.config = []

    def add_config(self, key, value):
        self.config.extend(["-c", key + "=" + value])
        return self

    def build(self):
        return self.config


class Client:

    def __init__(self, binary="lxc"):
        self.binary = binary

    def list_containers(self):
        return json.loads(subprocess.check_output([self.binary, "list", "--format", "json"]))

    def find_container(self, name):
        containers = self.list_containers()
        for container in containers:
            if container["name"] == name:
                return Container(container)
        return None

    def create_container(self, name, dist, config):
        command = [self.binary, "launch", dist, name]
        command.extend(config)
        cp = subprocess.run(command, shell=False)
        if cp.returncode != 0:
            return False
        return True

    def start_container(self, name):
        cp = subprocess.run([self.binary, "start", name])
        if cp.returncode != 0:
            return False
        return True

    def stop_container(self, name):
        cp = subprocess.run([self.binary, "stop", name])
        if cp.returncode != 0:
            return False
        return True

    def delete_container(self, name):
        cp = subprocess.run([self.binary, "delete", name])
        if cp.returncode != 0:
            return False
        return True

    def copy_file_to_container(self, name, host_file, container_file):
        print([self.binary, "file", "push", host_file, name + container_file])
        cp = subprocess.run([self.binary, "file", "push", host_file, name + container_file])
        if cp.returncode != 0:
            return False
        return True

    def execs_container(self, name, commands):
        file_name = "/tmp/" + _get_random_filename()
        file = open(file_name, "a")
        for command in commands:
            file.write(command+"\n")
        file.close()
        self.copy_file_to_container(name, file_name, file_name)
        result = self._exec_container(name, file_name)
        if os.path.exists(file_name):
            os.remove(file_name)
        return result

    def _exec_container(self, name, command):
        print([self.binary, "exec", name, "--", "/bin/bash", " " + command])
        cp = subprocess.run([self.binary, "exec", name, "--", "/bin/bash", command])
        if cp.returncode != 0:
            return False
        return True


class Container:

    def __init__(self, container_json):
        self.container_json = container_json

    def is_running(self):
        if self.status() == "Running":
            return True
        return False

    def is_stopped(self):
        if self.status() == "Stopped":
            return True
        return False

    def status(self):
        return self.container_json["status"]

    def type(self):
        return self.container_json["type"]

    def get_eth0_ipv6(self):
        if self.state()["network"] is None:
            return None
        eth0 = self.state()["network"]["eth0"]
        for addr in eth0["addresses"]:
            if addr["family"] == "inet6" and addr["scope"] == "global":
                ipv6 = addr["address"]
                return ipv6
        return None

    def state(self):
        return self.container_json["state"]

    def name(self):
        return self.container_json["name"]


def _get_random_filename():
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(12))
    return result_str
