# Env Manager
Env Manager provides an http api for automatically creating an lxc container, uploading artifacts and installing them
for testing from example the gitlab CI/CD

## Api:
### Authentication:
HttpBasic only one user currently
#### /lxd/container (POST)
Takes an json with the name of the lxc container as `"name"`.<br>
Creates the container if it not exist or starts it if it is not Running.
#### /lxd/container/upload (POST)
Takes an form with `containaer_name` and `artifact` as file and uploads the file to the container.<br>
#### /lxd/container/delete
Takes an json with the name of the lxc container as `"name"` and deletes it.<br>

