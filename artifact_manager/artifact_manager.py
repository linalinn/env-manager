from lxd.lxd import Client


class ArtifactManager:

    def __init__(self, artifact, container, config):
        self.artifact_config = config
        self.artifact = artifact
        self.client = Client()
        self.container = container

    def pre_copy(self):
        return self._exec("pre-copy")

    def _exec(self, entry):
        if entry in self.artifact_config.keys():
            if not self.client.execs_container(self.container, self.artifact_config[entry]):
                return False
        return True

    def copy(self):
        return self.client.copy_file_to_container(self.container, self.artifact, self.artifact_config["destination"])

    def post_copy(self):
        return self._exec("post-copy")

