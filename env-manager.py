import hashlib
import os
from json import JSONDecodeError

from artifact_manager.artifact_manager import ArtifactManager
from lxd.lxd import *
from dyndns.power_dns_admin import Client as DynDnsClient

from flask import Flask
from flask import request
from flask_httpauth import HTTPBasicAuth

auth = HTTPBasicAuth()
app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 64 * 1024 * 1024

global config
config = None
global client
client = Client()
global dns_client


@app.route('/lxd/container', methods=['POST'])
@auth.login_required
def lxd_container():
    data = None
    try:
        data = json.loads(request.data)
    except JSONDecodeError:
        return "Invalid Json", 400, {'Content-Type': 'text/plain'}

    if client.find_container(config["prefix"] + data["name"]) is None:
        if new_container(data["name"]):
            container = client.find_container(config["prefix"] + data["name"])
            if container:
                if not dns(data["name"], container.get_eth0_ipv6()):
                    return "container ok but dns failed", 500, {'Content-Type': 'text/plain'}
            return "created and started", 201, {'Content-Type': 'text/plain'}
        else:
            return "failed to created", 500, {'Content-Type': 'text/plain'}
    else:
        container = client.find_container(config["prefix"] + data["name"])
        if not container.is_running():
            if not client.start_container(container.name()):
                return "start failed", 500, {'Content-Type': 'text/plain'}
    ipv6 = container.get_eth0_ipv6()
    print(ipv6)
    if not dns(data["name"], container.get_eth0_ipv6()):
        return "container ok but dns update failed", 500, {'Content-Type': 'text/plain'}
    return "running", 200, {'Content-Type': 'text/plain'}


@app.route('/lxd/container/upload', methods=['POST'])
@auth.login_required
def lxd_container_upload():
    r = request
    if r.form and r.files:
        if "container_name" in r.form.keys() and "artifact" in r.files.keys():
            if client.find_container(config["prefix"] + r.form["container_name"]):
                if not os.path.exists(config["upload"] + "/" + config["prefix"] + r.form["container_name"]):
                    os.makedirs(config["upload"] + "/" + config["prefix"] + r.form["container_name"])
                artifact = r.files["artifact"]
                artifact_path = os.path.join(config["upload"], config["prefix"] + r.form["container_name"] + "/artifact")
                artifact.save(artifact_path)
                artifact_manager = ArtifactManager(artifact_path, config["prefix"] + r.form["container_name"], config["artifact"])
                if artifact_manager.pre_copy():
                    if artifact_manager.copy():
                        if not artifact_manager.post_copy():
                            return "failed on post_copy", 501, {'Content-Type': 'text/plain'}
                    else:
                        return "failed on copy", 501, {'Content-Type': 'text/plain'}
                else:
                    return "failed on pre_copy", 501, {'Content-Type': 'text/plain'}

            else:
                return "container dose not exist", 404, {'Content-Type': 'text/plain'}
        else:
            return "missing container_name and/or artifact", 400, {'Content-Type': 'text/plain'}
    return "artifact uploaded", 200, {'Content-Type': 'text/plain'}


@app.route('/lxd/container/delete', methods=['POST'])
@auth.login_required
def lxd_container_delete():
    data = None
    try:
        data = json.loads(request.data)
    except JSONDecodeError:
        return "Invalid Json", 400, {'Content-Type': 'text/plain'}

    if not client.find_container(config["prefix"] + data["name"]):
        return "container dose not exist", 404, {'Content-Type': 'text/plain'}

    container = client.find_container(config["prefix"] + data["name"])

    if not container.is_stopped():
        if not client.stop_container(container.name()):
            return "failed to stop", 500, {'Content-Type': 'text/ptlain'}

    if not client.delete_container(container.name()):
        return "failed to delete", 500, {'Content-Type': 'text/plain'}

    return "container deleted", 200, {'Content-Type': 'text/plain'}


@auth.verify_password
def verify_password(username, password):
    user = config["users"][0]
    if user["username"] == username and user["password"] == passwd_hash(password):
        return username


def passwd_hash(password):
    return hashlib.sha512(password.encode()).hexdigest()


def new_container(name):
    config_builder = ConfigBuilder()
    config_builder.add_config("limits.cpu", "1")
    config_builder.add_config("limits.memory", "1400MB")
    config_builder.add_config("limits.cpu.allowance", "60%")
    config_builder.add_config("security.nesting", "true")
    config_builder.add_config("security.privileged", "true")
    return client.create_container(config["prefix"] + name, config['dist'], config_builder.build())


def dns(name, ip):
    return dns_client.update(name + ".ipv6.u2s.cloud", ip)


if __name__ == '__main__':
    with open('config.json') as json_file:
        config = json.load(json_file)

    if not os.path.exists(config["upload"]):
        os.makedirs(config["upload"])

    dns_client = DynDnsClient(config["dyndns"]["username"],
                              config["dyndns"]["password"],
                              config["dyndns"]["host"],
                              verify=config["dyndns"]["verify"])

    app.run(port=config["port"])
