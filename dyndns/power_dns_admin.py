import requests


class Client:

    def __init__(self, user, passwd, host, verify=False):
        self.user = user
        self.passwd = passwd
        self.host = host
        self.verify = verify

    def update(self, sub, ip):
        r = requests.get(self.host + "/nic/update?hostname=" + sub + "&myip=" + ip,
                         auth=(self.user, self.passwd),
                         verify=self.verify)
        if r.status_code == 200:
            return True
        return False
