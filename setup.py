import setuptools

setuptools.setup(
    name="env-manager",
    version="0.0.2",
    author="LinaLinn",
    author_email="lina.cloud@outlook.de",
    description="lxd containers manager for testing your app",
    packages=setuptools.find_packages(),
    include_package_data=True,
    install_requires=["flask", "Flask-HTTPAuth", "requests"],
    scripts=['env-manager.py'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Mozilla Public License 2.0 (MPL 2.0)",
    ],
    python_requires='>=3.6',
)
